import Classes.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import javax.swing.JOptionPane;

class QuickStart {

    private static String titulo = "Compactação de Dados - Faculdade  UCL";

    public static void main(String[] args) throws UnsupportedEncodingException, IOException {	

        int opcao = MenuInicial();

        while(opcao != 0) {
            switch(opcao) { // Opções especificas da bilheteria enquanto estiver aberta
                case 1: 
                    // Compactar arquivo informado por parâmetro
                    // Arquivo de saida: compactado.txt
                    Huffman.Compactar( "teste_in.txt" );
                    break; 
                    
                case 2: 
                    // Compactar arquivo informado por parâmetro
                    // Arquivo de saida: descompactado.txt
                    Huffman.Descompactar( "compactado.txt" );
                    break; // Fechar bilheteria
            }

            opcao = MenuInicial();
        }
    }


    // Menu de opções
    public static int MenuInicial() {
        String opcao; // Variavel de leitura do menu
        int idOpcao; // Variavel de escolha das opções
        
        // MENU DE OPÇÕES
        opcao = JOptionPane.showInputDialog(null, 
                  "Bilheteria:                  \n"
                + " 1 - Compactar" +           "\n"
                + " 2 - Descompactar" +        "\n"
                + " 0 - Sair da aplicação       \n"
                + "=============================\n"
                + "Digite uma opção válida      \n"
                + "=============================\n",
                titulo, JOptionPane.QUESTION_MESSAGE);
        
        // Caso nenhum valor seja lido
        if(opcao == null)
            return 0;

        try{ // Caso algum valor seja lido
            idOpcao = Integer.parseInt(opcao); // Converter id de opção para o tipo inteiro
            
            if(idOpcao == 0)
                return 0;
            else if (idOpcao == 1 || idOpcao == 2) 
                return idOpcao;
            else // Caso a opção não exista
                throw new NumberFormatException();
        }
        catch(NumberFormatException e) {
            // Mostrar mensagem de erro de entrada
            JOptionPane.showMessageDialog(null, "Entrada inválida!", "Erro", JOptionPane.ERROR_MESSAGE);
            return MenuInicial(); // Repetir o método
        }
    }
}
