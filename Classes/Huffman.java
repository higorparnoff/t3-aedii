package Classes;

//Imports desse projeto
import Classes.Arvore.*;
import Classes.Arquivos.*;

//Demais imports
import java.nio.file.Files;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;


public class Huffman {

    /** Método que ler arquivo de entrada e cria o arquivo compactado
    *   @param path endereço do arquivo de entrada
    */
    public static void Compactar( String path ){
        try{
            // Ler o arquivo de entrada como um array de bytes
            byte[] bytesIn = Files.readAllBytes( new File(path).toPath() );

            //Monta a arvore, e gera os códigos binarios.
            Arvore arvore = new Arvore();
                arvore.Inserir(bytesIn);

            System.out.println("Estrutura interna da árvore: Compactação");
            arvore.Print();
            System.out.println(arvore.RetornarCabecalho());

            //Neste ponto já temos Arvore montado com os codigos gerados.

            // criarArquivoCompactado(cabecalho, dados binarios, referência da classe: Arquivo)
            criarArquivoCompactado(arvore.RetornarCabecalho(), Criptografar(bytesIn, arvore) , new File("compactado.txt") );
            
        } catch(Exception e){
            System.out.println("Não é possivel compactar arquivo: " + e.getMessage());
        }
        System.out.println();
    }


    /** Método que ler arquivo de entrada e cria o arquivo descompactado
    *   @param path endereço do arquivo de entrada (compactado)
    */
    public static void Descompactar( String path ){

        // Ler arquivos compactados e traduzilos de byte para char e agrupando em uma string
        try {
            String dados = LerarquivoCompactado(path);

            // Criar arquivo descompactado
            SalvarArquivoDescompactado(dados);
        } catch(Exception e){
            System.out.println("Não é possivel descompactar o arquivo: " + e);
        }
    }


    /** Monta o Stringão (Ex.:"10011010") que será salvo no arquivo compactado em bytes
    *   @param bytes dados lidos do arquivo de entrada em forma de bytes
    *   @param arvore Referência à arvore para buscar as coordenadas dos nós de cada byte
    */
    private static String Criptografar(byte[] bytes, Arvore arvore){
        String codBinario = "";
        for (byte b : bytes)
            //Busca as coordenadas de ada byte dentro do alfabeto da arvore
            codBinario += arvore.BuscarCodigoBinario(b);
        return codBinario;
    }


    
    /** Gera arquivo compactado:
    *   @param header Cabeçalho Ex: "{dado1,frequencia1;dado2,frequencia2}"
    *   @param binarios Binários do arquivo compactado Ex: "011001001"
    *   @param arquivo a ser compactado
    */
    private static void criarArquivoCompactado(final String header, final String binarios, final File arquivo) {
        
        int i  = -1, rangeBinario = binarios.length() / 8; // Variàveis de controle
        char[] binario = new char[rangeBinario]; // Criando vetor de armazenamento do tamanho da lista
        String restante = ""; // Restante de bits não agrupados

        // Converter binários para inteiro e guarda-los em um array
        for(String grupo : binarios.split("(?<=\\G.{8})")  ) { // Agrupando em 8 bits (UTF-8)
            // Se for possivel agrupar os próximos binarios em um octeto, se não guarde o restante
            i++;
            if(grupo.length() == 8)
                binario[i] = (char)toIntBinario(grupo);
            else
                restante = grupo;
        }

        // Escrevendo arquivo compactado
        try {
            // Limpando arquivo de saida e ecrevendo o cabeçalho apagando
            Arquivo file = new Arquivo(arquivo.getPath());
                file.Writeln(header+"("+restante+")", false); 

            // Concatenando dados binarios
            for(char bit: binario)
                file.Write(bit, true);
        }
        catch (Exception e) {
            System.out.println("Falha ao compactar: " + e.getMessage());
            e.printStackTrace();
        }
    }


    /** Ler e traduzir arquivo compactado
    *   @param path endereço do arquivo de entrada (compactado)
    */
    private static String LerarquivoCompactado(String path){

        Arvore arvore = new Arvore(); // Instanciar nova árvore
        int[] binarios; // Array de armazenamento da trazução da leitura
        Arquivo arq = new Arquivo(path); // Arquivo de escrita

        // Ler cabeçalho e identificar binarios restantes armazenados
        String cabecalho = arq.ReadLine(); 
        String restante = cabecalho.substring( cabecalho.indexOf( "(" )+1, cabecalho.indexOf( ")" ) ); 
        cabecalho = cabecalho.substring( cabecalho.indexOf( "{" )+1, cabecalho.indexOf( "}" ) );


        ///////////////////////////////////////////////////////////
        // Montar nova árvore a partir da matriz de nós criada - INICIO

        char[] caminho; // coordenadas do nó a ser inserido na árvore
        int contNos = 0; // Variável de controle de contagem
        noLista inicialL = null, ref = null; // Atributos da lista interna da nova árvore

        // Criar matriz com os dados de todos os nós armazenados no cabeçalho
        String[][] nos = new String[cabecalho.split(";").length][3];
        for (String dados : cabecalho.split(";")) {
            nos[contNos] = dados.split(",");
            contNos++;
        }

        // Encaixar os nós pelas coordenadas armazenadas no cabeçalho
        noArvore newArvore = contNos > 0? new noArvore(): null; // Raiz da nova árvore
        noArvore aux;
        for (int cont = 0; cont < contNos; cont++) {
            aux = newArvore;
            
            // Percorrendo nova árvore
            caminho = nos[cont][2].toCharArray();
            for (int j = 0; j < caminho.length; j++) {
                if(caminho[j] == '0') {
                    if(aux.getEsquerda() == null) 
                        aux.setEsquerda(new noArvore());
                    aux = aux.getEsquerda();
                } else {
                    if(aux.getDireita() == null)
                        aux.setDireita(new noArvore());
                    aux = aux.getDireita();
                }
            }

            // Criando novo nó ná posição indicada
            // new noArvore((byte) dado, (int) frequencia)
            // noArvore.setCodigoBinario((String) codigoBinario)
            aux.setFolha(new noArvore((byte)Integer.parseInt(nos[cont][0]), Integer.parseInt(nos[cont][1])));
            aux.setCodigoBinario(nos[cont][2]);

            // Atualizar referências para a próxima inserção
            if(inicialL == null){
                inicialL = new noLista(aux);
                ref = inicialL;
            }else {
                ref.setPosterior(new noLista(aux));
                ref = ref.getPosterior();
            }
            
        }

        // Finalizar montagem da nova árvore
        arvore = new Arvore(newArvore, inicialL, ref, contNos);
        
        // Imprimir estrutura da árvore montada
        System.out.println("Estrutura interna da árvore: Descompactada");
        arvore.Print();

        // Montar nova árvore a partir da matriz de nós criada - FIM
        ///////////////////////////////////////////////////////////

        // Montar o stringão com os dados descompactados a partir da nova árvore
        char[] leitura;
        String stringao = "";
        try{ 
            leitura = arq.ReadLine().toCharArray(); 
            while(leitura != null) {
                for (char c : leitura ) {
                    stringao += toBinarioString(c);
                }
                leitura = arq.ReadLine().toCharArray(); 
            }
        } catch(Exception e){}
    
        // Montar array de binarios: compactados + binarios restantes guardados no cabeçalho
        binarios = new int[ stringao.length() + restante.length() ]; // Array de coordenadas binarias para a árvore
        int i = 0; // Variável de controle
        for (char c : stringao.toCharArray() ) { // Dados descompactados
            binarios[i] = c=='1'? 1: 0;
            i++;
        }
        for(char c: restante.toCharArray()){ // Dados restantes do cabeçalho
            binarios[i] = c == '1'? 1: 0;
            i++;
        }

        // Buscar e retornar informações guardadas na árvore indicadas pelas coordenadas binarias
        return arvore.Decodificar(binarios);
    }


    /** Converte inteiro para binario
    *   @param bits valor numerico
    *   @return Código binário de 8 bits correspondente ao valor de entrada
    */
    private static String toBinarioString(int bits) {
        String codBinario = "";
        int den = 128;

        for (int i = 0; i < 8; i++) {
            codBinario += bits >= den? 1: 0;
            bits -= bits >= den? den: 0;
            den = den / 2;
        }
        return codBinario;
    }



    /** Converte inteiro para binario
    *   @param bits Códigos binário de 8 bits
    *   @return Valor numerico equivalente ao código binário de entrada
    */
    private static int toIntBinario(String bits) {
        char[] bit = bits.toCharArray();
        return  (bit[7] == 49? 1:0) + (bit[6] == 49? 2:0) + (bit[5] == 49? 4:0) + (bit[4] == 49? 8:0) + 
                (bit[3] == 49? 16:0) + (bit[2] == 49? 32:0) + (bit[1] == 49? 64:0) + (bit[0] == 49? 128:0);
    }
    

    private static void SalvarArquivoDescompactado(String dados) {

        // Bytes de entrada a serem salvos
        byte[] bytes = dados.getBytes();

        // Criar o arquivo descompactado
        File file = new java.io.File("descompactado.txt"); // Endereço de saida
        FileOutputStream out; // Objeto file de escrita
        try {
            // Escrever os dados no arquivos
            out = new FileOutputStream(file);
            out.write(bytes);
            out.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}