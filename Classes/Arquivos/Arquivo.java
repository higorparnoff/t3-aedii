package Classes.Arquivos;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;


public class Arquivo {

  // Atributos da classe
  private FileReader fileReader = null;
  private BufferedReader bufferedReader = null;
  private boolean arquivoIsNotOpen;

  private FileWriter fileWriter = null;
  private PrintWriter printwriter = null;

  private String pathArquivo;


  /** Construtor da classe arquivos
  *   @param n Caminho do arquivo
  */
  public Arquivo(String n){
      pathArquivo = n;
      arquivoIsNotOpen = true;
  }
  

  ////////////////////////////////////////////////////
  // MÉTODOS DA CLASSE

  // Método que retorna um array com cada byte do arquivo informado como parâmetro 
  public static byte[] readAllBytes(String path) throws UnsupportedEncodingException, IOException{
      File file = new File(path);
      
      // Método abaixo recebe como parâmetro um objeto Path. Utilizei método da classe file para criar objeto Path
      return Files.readAllBytes( file.toPath() );
  }


  // Lê linha por linha, deve ser lido até o fim para fechar o arquivo.
  public String ReadLine( ){
      String linha;

      try {
          if(arquivoIsNotOpen){
              fileReader = new FileReader(pathArquivo);
              bufferedReader = new BufferedReader(fileReader);
              arquivoIsNotOpen = false;
          }
          // Ler linha
          if ((linha = bufferedReader.readLine()) != null) {
              return linha;
          }
          Close();
      }catch (IOException e) {
          System.out.println("Exception ao ler arquivo: " + e.getMessage());
      }

      arquivoIsNotOpen = true;
      return null;
  }


  // Fechar arquivos e instâncias de leitura
  public void Close(){
      try {
          fileReader.close();
          bufferedReader.close();
      }catch (IOException e) {
          System.out.println("Exception ao fechar arquivo: " + e.getMessage());
      }
  }


  // Escrever uma linha com quebra de linha no final
  public void Writeln(String texto, boolean concatenar){ 
      try {
          fileWriter = new FileWriter(pathArquivo, concatenar);
          printwriter = new PrintWriter(fileWriter);

          // Escrever linha
          printwriter.println(texto);

          printwriter.close();
          fileWriter.close();
      } catch (IOException e) {
          System.out.println("Exception ao escrever no arquivo: " + e.getMessage());
      }
  }


  // Escrever texto com concatenamento
  // concatenar = true: escreve em cima do conteudo ja escrito
  // concatenar = true: apaga o arquivo de saida antes de escrever
  public void Write(char texto, boolean concatenar){ 
      try {
          fileWriter = new FileWriter(pathArquivo, concatenar);
          printwriter = new PrintWriter(fileWriter);

          // Escrever linha
          printwriter.print(texto);

          printwriter.close();
          fileWriter.close();
      } catch (IOException e) {
          System.out.println("Exception ao escrever no arquivo: " + e.getMessage());
      }
  }


  // O arquivo está fechado?
  public boolean isClosed(){
      return this.arquivoIsNotOpen;
  }
}