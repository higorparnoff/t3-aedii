package Classes.Arvore;

public class Arvore {

    ////////////////////////////////////////////////
    // Atributos da classe

    // Atributos da árvore
    private noArvore raiz;
    private noArvore[] matrizNos;

    // Atributos da lista interna
    private noLista noInicialL;
    private noLista noFinalL;
    private int indexL = -1;


    ////////////////////////////////////////////////
    // CONSTRUTORES

    public Arvore(){
        // Atributos da árvore
        this.raiz = null;
        this.matrizNos = new noArvore[0];

        // Atributos da lista interna
        this.noInicialL = null;
        this.noFinalL = null;
        this.indexL = -1;
    }

    public Arvore(noArvore r, noLista i, noLista f, int n){
        // Atributos da árvore
        this.raiz = r;
        this.matrizNos = new noArvore[0];

        // Atributos da lista interna
        this.noInicialL = i;
        this.noFinalL = f;
        this.indexL = n - 1;

        // Gerar matriz de nós
        this.GerarArrayNos(); 
    }


    ////////////////////////////////////////////////
    // GETTERS AND SETTERS
    private noArvore getRaiz(){
        return this.raiz;
    }

    private int getIndexL(){
        return this.indexL;
    }

    public noArvore[] getMatrizNos(){
        return this.matrizNos;
    }
    //=============================================
    private void setRaiz(noArvore r){
        this.raiz = r;
    }

    private void setIndexL(int i){
        this.indexL = i;
    }


    ////////////////////////////////////////////////
    // MÉTODOS DA ARVORE

    /** A arvore está vazia?
    *   @return true ou false 
    */
    public boolean isVazia() {
        return this.getRaiz() == null? true: false;
    }


    /** Inserir conjunto de nós na árvore atravéz de uma lista interna
    *   @param no novo nó que serão inserido na arvore
    */
    public void Inserir(noArvore no){

        if(no.getDado() != 13){
            // Adicionando novo nó na lista interna de nós
            // Isso garante que nenhum nó estará repetido dentro da árvore
            this.InserirNoLista( no );

            // Adicionando nó na árvore
            if(this.isVazia())
                this.setRaiz(no);
            else{
                this.BalancearNovoNo(this.getRaiz(), no);
                this.GerarCodigo();
            }
        }
    }


    /** Inserir conjunto de nós na árvore atravéz de uma lista interna
    *   @param dado array de bytes que serão inseridos na árvore
    */
    public void Inserir(byte[] dado){

        // Criando nós da arvore e agrupando em uma lista.
        // Nós repetidos não são enfileirados, mas o atual tem a frequência agregada em uma unidade
        // Isso garante que nenhum nó estará repetido dentro da árvore
        for (byte b : dado) 
            if(b != 13 && b != 0) // Eliminando bytes problematicos: quebras de linha e null
                this.InserirNoLista( new noArvore(b) );

        // Transferindo nós da lista interna para a árvore
        // AcessarLista(): retorna um noArvore guardado dentro da lista interna
        this.setRaiz(null); // Resetar árvore
        for(int i = 0; i <= this.getIndexL(); i++){
            if(this.isVazia())
                this.setRaiz(this.AcessarLista(i));
            else
                BalancearNovoNo(this.getRaiz(), this.AcessarLista(i));
        }

        // Gerar dentro de cada nó da árvore o código binário que informa sua localização exata a partir do nó raiz
        this.GerarCodigo();
    }


    /** Balanceamento de um novo nó na árvore pelo nível de frequência
    *   @param noRef inicia na raiz e permite acessar os demais nós da árvore
    *   @param novoNo novo nó a ser inserido na árvore
    */
    private void BalancearNovoNo( noArvore noRef, noArvore novoNo ) {

        // SE O NÓ DE REFERÊNCIA FOR NÓ PAI
        if(!noRef.isFolha()){
            int freqNewNo = novoNo.getFrequencia();

            // Caso a frequência do nó não faça parte desse nível, avançar para os nós filhos
            if(freqNewNo < noRef.getDireita().getFrequencia())
                this.BalancearNovoNo(noRef.getDireita(), novoNo);
            else if(freqNewNo == noRef.getDireita().getFrequencia())
                noRef.getDireita().setPai(novoNo);
            else if(freqNewNo < noRef.getEsquerda().getFrequencia())
                this.BalancearNovoNo(noRef.getEsquerda(), novoNo);
            else if(freqNewNo == noRef.getEsquerda().getFrequencia())
                noRef.getEsquerda().setPai(novoNo);
           
            else // Caso seja um nó inexistente e possua uma frequência conpativel com o nível atual 
                noRef.setPai(novoNo);       
        }
        else // SE O NÓ DE REFERÊNCIA FOR NÓ FOLHA
            noRef.setPai(novoNo);
    }


    /** GerarCodigo(): Gerar dentro de cada nó da árvore o código binário que informa sua localização exata a partir do nó raiz
    *   
    *   Codificar(): Método recursivo que percorre a árvore inserindo as coordenadas de cada nó
    *   @param noRef nó atual, iniciando da raiz até os nós folhas
    *   @param codigo coordenada do nó anterior ao noReferente, é incrementado com a recursividade
    */
    public void GerarCodigo(){
        Codificar(this.getRaiz(), "");
        this.GerarArrayNos(); // Gerar matriz de nós
    }    
    private static void Codificar(noArvore noRef, String codigo){
        //Se o nó da esquerda for folha seta o código, caso contrário chama esta função recursivamente.
        if( noRef.getEsquerda().isFolha() )
            noRef.getEsquerda().setCodigoBinario(codigo + "0");
        else 
            Codificar(noRef.getEsquerda(), codigo + "0");

        //Se o nó da direita for folha seta o código, caso contrário chama esta função recursivamente.
        if( noRef.getDireita().isFolha() )
            noRef.getDireita().setCodigoBinario(codigo + "1");
        else
            Codificar(noRef.getDireita(), codigo + "1");
    }


    /** GerarArrayNos(): Gera um array que guarda a referencia de todos os nós folhas
    *   sua função é acelerar o acesso às coordenadas de cada nó para inserir no cabeçalho
    *   
    *   MontarArrayNos(): Método recursico para atender ao método GerarArrayNos()
    */
    private void GerarArrayNos() {
        this.matrizNos = new noArvore[this.getIndexL() + 1];
        this.MontarArrayNos(this.getRaiz(), 0);
    }
    private int MontarArrayNos(noArvore no, int index){
        if(!no.isFolha()){
            index = this.MontarArrayNos(no.getEsquerda(), index);
            index = this.MontarArrayNos(no.getDireita(), index); 
        } else {
            this.matrizNos[index] = no;
            return index + 1;
        }
        return index;
    }


    /** Busca no arrayNos as coordenadas de um determinado caracter
    *   @param dado caracter a ser procurado
    *   @return código binario que informa a coordenada do dado na árvore
    */
    public String BuscarCodigoBinario(byte dado){

        // Se não for um caracter problematico: quebra de linha e null
        if(dado == 13 || this.getMatrizNos().length == 0)
            return "";

        // Buscar coordenadas no arrayNos
        noArvore[] auxA = this.getMatrizNos();
        try{
            for(int i = 0; i <= this.getIndexL(); i++)
                if(auxA[i].getDado() == dado)
                    return auxA[i].getCodigoBinario();
        } 
        catch(Exception e) {}

        System.out.println("ERRO: Byte não encontrado ao compactar." + dado);

        return "";
    }


    /** Busca na árvore o dado presente nas coordenadas informadas
    *   @param caminho coordenadas de um nó na árvore
    *   @return dado do nó
    */
    public String Decodificar(int[] caminho){

        noArvore no = this.getRaiz();
        String result = "";
        for (int c : caminho) {
            // Percorer os nós da árvore
            no = c == 0? no.getEsquerda(): no.getDireita(); 
    
            // Caso incontre o nó, que obrigatóriamente deve ser nó folha
            if ( no.isFolha() ){
                //Converte o dado em Byte para Char de acordo com a tabela ASCII
                result += (char)no.getDado();
                no = this.getRaiz();    
            }
    
        }
        //Retorna uma String com os Bytes já convertidos
        return result;
    }


    // Printar na tela todos os nós da árvore
    public void Print() {
        if(!this.isVazia())
            // Percorrer por todos os nós da árvore buscando os nós folhas
            Percorrer(this.getRaiz());
        else 
            System.out.println("A árvore está vazia");
    }


    /** Método recursivo para acessar todos os nós da árvore
    *   @param no nó de referencia que parte da raiz
    */
    private void Percorrer(noArvore no) {
        
        if(!no.isFolha()){
            this.Percorrer(no.getEsquerda());
            this.Percorrer(no.getDireita()); 
        } else {
            // Imprimir os dados do nó folha
            System.out.println("Byte: " + no.getDado() + " - Caracter: " + (no.getDado() == 10? "br": (char)no.getDado()) + " - Freq: " + no.getFrequencia() + " - Caminho: " + no.getCodigoBinario());
        }
    }

        
    /** Monta e retorna o cabeçalho com os dados de todos os nós folhas
    *   @return cabeçalho em formato similar a um JSON
    *
    *   MontarCabecalho(): Método recursivo que atende ao método RetornarCabecalho()
    */
    public String RetornarCabecalho() {
        String nos = MontarCabecalho(this.getRaiz());
        return "{" + nos.substring(0, nos.length() - 1) + "}";
    }
    private String MontarCabecalho(noArvore no) {
        
        if(!no.isFolha())
            return this.MontarCabecalho(no.getEsquerda()) + this.MontarCabecalho (no.getDireita()); 
        else
            return no.getDado() + "," + no.getFrequencia() + "," + no.getCodigoBinario() + ";";
    }    

    //====================================================================================================
    
    /////////////////////////////////////////////
    // MÉTODOS DA LISTA INTERNA

    // A lista interna da árvore está vazia?
    private boolean isListaVazia(){
        return this.getIndexL() == -1? true: false;
    }


    /** Retorna um noArvore guardado na lista por seu index
    *   @param index posição na lista interna do noArvore desejado
    */
    private noArvore AcessarLista(int index){

        if( this.getIndexL() < index )
            return null;

        //Chegar no nó desejado
        noLista ref = this.noInicialL; //Variável Auxiliar
        for(int g = 0; g < index; g++)
            ref = ref.getPosterior();

        return ref.getItem();
    }


    /** Inserir elemento na lista interna.
    *   @param no inserir novo nó ou incrementar um nó existente na lista interna da árvore
    */
    private boolean InserirNoLista( noArvore no){

        // Caso esteja vazia, inserir seu primeiro nó
        if(this.isListaVazia()){
            this.noInicialL = new noLista( no );
            this.noFinalL = this.noInicialL;
            this.setIndexL(this.getIndexL() + 1);
            return true;
        }

        // Referência para percorer os nós da lista
        noLista ref = this.noInicialL;
        
        // Checar se o nó já existe dentro da lista
        for(int i = 0; i <= this.getIndexL() ; i++){
            
            if( ( ref.getItem().getDado() == no.getDado() ) && ( no.isFolha() ) ){
                ref.getItem().upFrequencia();
                return true;
            }
            if( this.getIndexL() == i )
                break;
            ref = ref.getPosterior();
        }

        //Aqui estou no nó anterior do que desejo inserir

        //Nó que vou inserir
        noLista n = new noLista( no );

        // Se a lista possuir apenas um nó
        if(this.getIndexL() == 0){
            n.setPosterior(ref);
            this.noInicialL = n;
        }
        else{
            //Verificar se estou no último elemento da lista!
            if( ref.getPosterior() != null)
                n.setPosterior( ref.getPosterior() );
            else{
                //Estou no noFinalL elemento da lista!
                n.setPosterior(null);
                this.noFinalL = n;
            }
            ref.setPosterior(n);
        }
            
        // Atualizar contagem de nós
        this.setIndexL(this.getIndexL() + 1);
        return true;
    }

}