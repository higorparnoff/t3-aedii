package Classes.Arvore;

public class noArvore {
    
    ////////////////////////////////////////////////
    // Atributos da classe
    private int frequencia;
    private byte dado;
    private String codigoBinario;

    private noArvore esquerda;
    private noArvore direita;


    ////////////////////////////////////////////////
    // CONSTRUTORES

    //Construtor de nó nulo
    public noArvore(){
        this.setDado((byte)0);
        this.setEsquerda(null);
        this.setDireita(null);
        this.setFrequencia(0);
        this.setCodigoBinario("");
    }

    //Construtor de nó folha
    public noArvore(byte d){
        this.setDado(d);
        this.setEsquerda(null);
        this.setDireita(null);
        this.setFrequencia(1);
        this.setCodigoBinario("");
    }

    //Construtor de nó com frequencia
    public noArvore(byte d, int f){
        this.setDado(d);
        this.setEsquerda(null);
        this.setDireita(null);
        this.setFrequencia(f);
        this.setCodigoBinario("");
    }


    ////////////////////////////////////////////////
    // Getters and Setters
    public noArvore getEsquerda(){
        return this.esquerda;
    }

    public noArvore getDireita(){
        return this.direita;
    }

    public byte getDado(){
        return this.dado;
    }

    public int getFrequencia(){
        return this.isFolha()? this.frequencia: this.getDireita().getFrequencia() + this.getEsquerda().getFrequencia();
    }

    public String getCodigoBinario(){
        return this.codigoBinario;
    }
    //=============================================
    public void setEsquerda(noArvore e){
        this.esquerda = e;
    }

    public void setDireita(noArvore d){
        this.direita = d;
    }

    private void setDado(byte d){
        this.dado = d;
    }

    private void setFrequencia(int f){
        if(this.isFolha())
            this.frequencia = f;
    }
    
    public void setCodigoBinario(String cb){
        this.codigoBinario = cb;
    }


    ////////////////////////////////////////////////
    // MÉTODOS DA CLASSE

    // Atualizar frequência
    public void upFrequencia() {
        this.setFrequencia( this.getFrequencia() + 1 );
    }

    // É folha? = Possui algum nó filho?
    public boolean isFolha() {
        return this.getEsquerda() == null && this.getDireita() == null;
    }

    // Método que transforma esse nó folha em um nó pai
    // Seus filhos serão: no(parâmetro de entrada) e seu clone(novo nó folha gerado com os dados desse nó)
    // Entenda "esse nó" como o nó que executa o método
    public void setPai(noArvore no) {
        noArvore aux = new noArvore(this.getDado());
        aux.setFrequencia(this.getFrequencia());
        aux.setDireita(this.getDireita());
        aux.setEsquerda(this.getEsquerda());

        if(no.getFrequencia() >= this.getFrequencia()){
            this.setEsquerda(no);
            this.setDireita(aux);
        }else{
            this.setEsquerda(aux);
            this.setDireita(no);
        }
        this.setDado((byte)0);
        this.setCodigoBinario("");
    }

    // Método que transforma esse nó folha em um nó pai
    // Uma ação que desfaz, caso necessário, o método setPai()
    public void setFolha(noArvore no) {
        this.setDado(no.getDado());
        this.setEsquerda(null);
        this.setDireita(null);
        this.setFrequencia(no.getFrequencia());
        this.setCodigoBinario("");
    }
}