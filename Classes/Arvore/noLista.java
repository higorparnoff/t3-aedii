package Classes.Arvore;

public class noLista {
    
    // Atributos da classe
    private noArvore item;
    private noLista anterior;
    private noLista posterior;


    /** Construtor da classe noLista
     *  @param p noArvore - noArvore que será listada.
     */
    public noLista(noArvore p){
        this.item = p;
    }


    ///  GETTERS AND SETTERS   ///
    public noArvore getItem(){
        return this.item;
    }
    
    public noLista getAnterior(){
        return this.anterior;
    }
    
    public noLista getPosterior(){
        return this.posterior;
    }
    //=============================================
    public void setItem(noArvore p){
        this.item = p;
    }

    public void setAnterior(noLista n){
        this.anterior = n;
    }
    
    /**
     * Método para setar o atributo posterior.
     * @implNote
     * Ao setar o posterior, automaticamente, o posterior terá como anterior, o objeto que chamou este método.
     * @param n noLista - Objeto que será o posterior;
     */
    public void setPosterior(noLista n){
        this.posterior = n;
        if(n != null){
            n.setAnterior(this);
        }
    }
}